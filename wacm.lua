local lfs = require("lfs")

UserName = ""

local function read_file(path)
  local file = io.open(path, "r")
  file:read()
  local content = file:read("*all")
  file:close()
  return content
end

local function parse_date_time(str)
  local day, month, year, hour, minute = str:match("(%d+).(%d+).(%d+), (%d+):(%d+)")
  if day and month and year and hour and minute then
    year = tonumber(year) + 2000
    local date_time = os.time({ year = year, month = month, day = day, hour = hour, min = minute })
    return date_time
  end
end

local files = {}
for file in lfs.dir(".") do
  if lfs.attributes(file, "mode") == "file" and file:match("%.txt$") and file:match(UserName) then
    local path = lfs.currentdir() .. "/" .. file
    local content = read_file(path)
    local date_time_str = content:match("^([^\n]+)\n")
    if date_time_str then
      local date_time = parse_date_time(date_time_str)
      if date_time then
        table.insert(files, { path = path, date_time = date_time })
      end
    end
  end
end

table.sort(files, function(a, b) return a.date_time < b.date_time end)

local output_file = io.open("wa.txt", "w")
local lines = {}
for _, file in ipairs(files) do
  local content = read_file(file.path)
  for line in content:gmatch("([^\n]*)\n") do
    if not lines[line] then
      lines[line] = true
      output_file:write(line .. "\n")
    end
  end
end
output_file:close()
